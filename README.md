# (API) DATABASE

Create table to students information

### Preview

displays estudent information

![base_de_de_datos](/uploads/32f4024e9464b0838a849ae167694fbc/base_de_de_datos.png)

### Basic operation

- read

#### Database connection


![CONEXION_LOCAL_HOST](/uploads/c3b19eff18c2e7ce33b63943ec040f3a/CONEXION_LOCAL_HOST.png)

#### Terminal commands

 insert students

 ![añadir_usuario](/uploads/ccced51112dceec38f7ea2d09e370d75/añadir_usuario.png)
 
 display students in console
 
 
![terminal](/uploads/1c6f886543465c9f52fd4040b367d056/terminal.png)

